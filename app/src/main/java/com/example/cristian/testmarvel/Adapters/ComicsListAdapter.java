package com.example.cristian.testmarvel.Adapters;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dinuscxj.progressbar.CircleProgressBar;
import com.example.cristian.testmarvel.Entidades.Comic;
import com.example.cristian.testmarvel.EntidadesComicsJson.Result;
import com.example.cristian.testmarvel.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Cristian on 9 feb 2017.
 */
public class ComicsListAdapter extends RecyclerView.Adapter<ComicsListAdapter.ComicsViewHolder> implements Filterable{
    private List<Result> items;
    private List<Result> filterable;
    Context context;
    private static OnClickListener  clickListener;
    public static final int TAG_NO_FAVORITO =1;
    public static final int TAG_FAVORITO =2;
    Filter filter;

    @Override
    public int getItemCount() {
        return filterable.size();
    }

    @Override
    public ComicsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_item_comic, viewGroup, false);

        return new ComicsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ComicsViewHolder viewHolder, int i) {

        Result comic = filterable.get(i);
        if (comic.getImages().size() >0)
            Glide.with(context).load(comic.getImages().get(0).getPath()+"."+comic.getImages().get(0).getExtension())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.error)

                    .placeholder(R.drawable.loading)
                    .into(viewHolder.imagen);
        else Glide.with(context).load(comic.getThumbnail().getPath()+"."+comic.getThumbnail().getExtension())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.error)

                .placeholder(R.drawable.loading)
                .into( viewHolder.imagen);


        viewHolder.titulo.setText(comic.getTitle());

        if (comic.getPrices().get(0).getPrice()!=0)
            viewHolder.precio.setText("$ "+String.valueOf(comic.getPrices().get(0).getPrice()));
        else viewHolder.precio.setText("AGOTADO");


        //traer de la bd si es favorito
        List<Comic> comicBD = Comic.find(Comic.class," idComic =  ? ", String.valueOf(comic.getId()));
        if (comicBD.size()>0)
            viewHolder.favorito.setImageResource(android.R.drawable.star_big_on);
        else   {
            viewHolder.favorito.setVisibility(View.INVISIBLE);
        }





    }

    public void setOnItemClickListener(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public Result getItem(int position) {
        return this.filterable.get(position);
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new ComicsFilter();
        return filter;
    }

    public interface OnClickListener {
        void onItemClick(int position, View v);
        void favorito(int position,View v);

    }
    public void clearItemsAll() {
        if (filterable != null){
            filterable.clear();
            notifyDataSetChanged();
        }
    }



    public ComicsListAdapter(List<Result> items,Context context) {
        this.items = items;
        this.context = context;
        this.filterable = items;
    }


    public static class ComicsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.imagen) public ImageView imagen;
        @BindView(R.id.titulo) public TextView titulo;
        @BindView(R.id.precio) public TextView precio;
        @BindView(R.id.favorito) public ImageView favorito;


        public ComicsViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
            v.setOnClickListener(this);

            favorito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.favorito(getAdapterPosition(),v);

                }
            });
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    private class ComicsFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String buscar = ((String) constraint).toUpperCase();
            List<Result> add = new ArrayList<>();
            FilterResults results1 = new FilterResults();

            if (buscar.length() >0){

                for (Result r : items){
                    if (r.getTitle().toUpperCase().contains(buscar))
                        add.add(r);
                }
                results1.values = add;
                results1.count = add.size();

            } else {
                results1.values = items;
                results1.count = items.size();
            }

            return results1;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterable = (List<Result>) results.values;
            notifyDataSetChanged();
        }
    }




}