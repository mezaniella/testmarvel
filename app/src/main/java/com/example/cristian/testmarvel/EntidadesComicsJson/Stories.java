
package com.example.cristian.testmarvel.EntidadesComicsJson;

import com.orm.SugarRecord;

import java.util.List;

public class Stories extends SugarRecord{

    private long available;
    private String collectionURI;
    private List<Item_> items = null;
    private long returned;

    public Stories(){

    }

    public long getAvailable() {
        return available;
    }

    public void setAvailable(long available) {
        this.available = available;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    public List<Item_> getItems() {
        return items;
    }

    public void setItems(List<Item_> items) {
        this.items = items;
    }

    public long getReturned() {
        return returned;
    }

    public void setReturned(long returned) {
        this.returned = returned;
    }

}
