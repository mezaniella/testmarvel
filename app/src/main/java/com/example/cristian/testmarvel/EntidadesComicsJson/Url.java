
package com.example.cristian.testmarvel.EntidadesComicsJson;


import com.orm.SugarRecord;

public class Url {

    private String type;
    private String url;

    public Url(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
