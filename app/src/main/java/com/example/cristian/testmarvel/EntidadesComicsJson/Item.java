
package com.example.cristian.testmarvel.EntidadesComicsJson;


import com.orm.SugarRecord;

public class Item extends SugarRecord{

    private String resourceURI;
    private String name;
    private String role;

    public Item(){

    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
