package com.example.cristian.testmarvel.Interfaz;

import com.example.cristian.testmarvel.EntidadesComicsJson.Pricipal;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by grite on 9/2/2017.
 */

public interface MarvelApi {
     String path_comics="v1/public/comics";
    String path_series="v1/public/series";

    @GET(path_comics)
    Call<Pricipal> getPrincipal(
            @Query(("ts")) String user,
            @Query(("apikey")) String apikey,
            @Query(("hash")) String hash,
            @Query(("offset")) String offset,
            @Query(("limit")) String limit
    );

    @GET(path_comics+"/{idComic}")
            Call<Pricipal> getComic(
            @Path("idComic") String idComic,
            @Query(("ts")) String ts,
            @Query(("apikey")) String apikey,
            @Query(("hash")) String hash

    );


    @GET(path_series+"/{idComic}")
    Call<Pricipal> getSeries(
            @Path("idComic") String idComic,
            @Query(("ts")) String ts,
            @Query(("apikey")) String apikey,
            @Query(("hash")) String hash

    );



    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://gateway.marvel.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();



}
