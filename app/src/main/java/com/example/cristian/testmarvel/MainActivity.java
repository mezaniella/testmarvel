package com.example.cristian.testmarvel;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.cristian.testmarvel.Fragments.ComicFragments;
import com.example.cristian.testmarvel.Fragments.FavoritosFragments;
import com.example.cristian.testmarvel.Fragments.ListComicsFragments;
import com.example.cristian.testmarvel.Interfaz.SetFragments;
import com.mikhaellopez.circularimageview.CircularImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SetFragments {

    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.toolbar) Toolbar toolbar;

    public Toolbar getToolbar(){
        return this.toolbar;
    }

    public static final int LIST_COMICS_FRAGMENTS=1;
    public static  final int COMIC_FRAGMENTS=2;
    public static final int FAVORITOS_COMICS=3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        setFragment(LIST_COMICS_FRAGMENTS);
        cargarDatos();
    }

    private void cargarDatos() {
        try {
            SharedPreferences prefs =
                    getSharedPreferences("app", Context.MODE_PRIVATE);

            String nombre = prefs.getString("nombre", "default");
            String uriFoto = prefs.getString("uriPicture", "");

            View headerLayout = navigationView.getHeaderView(0);

            CircularImageView ivHeaderPhoto = (CircularImageView) headerLayout.findViewById(R.id.foto_profile);
            TextView nombre_usuario = (TextView) headerLayout.findViewById(R.id.nombew_profile);

            nombre_usuario.setText(nombre);


            Glide.with(this).load(uriFoto)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.error)
                    .into(ivHeaderPhoto);
        } catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.list_comics) {
            setFragment(LIST_COMICS_FRAGMENTS);
        } else if (id == R.id.favoritos_comics) {
            setFragment(FAVORITOS_COMICS);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setFragment (int fragments){
        Fragment fragment = null;
        switch (fragments){
            case LIST_COMICS_FRAGMENTS:

                fragment = ListComicsFragments.newInstance(null);
                break;
            case FAVORITOS_COMICS:

                fragment = FavoritosFragments.newInstance(null);
                break;
        }

        transicion(fragment);
    }

    public void transicion(Fragment fragment){

        if (fragment!=null) {
            FragmentTransaction trans = getFragmentManager().beginTransaction();

            trans.replace(R.id.frame_reemplazar, fragment);

            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            //trans.addToBackStack(null);


            trans.commit();
        }
    }

    @Override
    public void setFragmentComic(int frament, long idComic) {
        Bundle bundle = new Bundle();
        bundle.putLong("idComic",idComic);

        Fragment fragment = ComicFragments.newInstance(bundle);

        if (fragment!=null) {
            FragmentTransaction trans = getFragmentManager().beginTransaction();

            trans.replace(R.id.frame_reemplazar, fragment);

            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            trans.addToBackStack(null);


            trans.commit();

        }

    }
}
