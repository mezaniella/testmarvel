
package com.example.cristian.testmarvel.EntidadesComicsJson;

import com.orm.SugarRecord;

import java.util.List;

public class Creators extends SugarRecord{

    private long available;
    private String collectionURI;
    private List<Item> items = null;
    private long returned;

    public Creators(){

    }

    public long getAvailable() {
        return available;
    }

    public void setAvailable(long available) {
        this.available = available;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public long getReturned() {
        return returned;
    }

    public void setReturned(long returned) {
        this.returned = returned;
    }

}
