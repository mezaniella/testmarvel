package com.example.cristian.testmarvel.Entidades;

import com.example.cristian.testmarvel.Adapters.ComicsListAdapter;
import com.example.cristian.testmarvel.EntidadesComicsJson.Characters;
import com.example.cristian.testmarvel.EntidadesComicsJson.Creators;
import com.example.cristian.testmarvel.EntidadesComicsJson.Image;
import com.example.cristian.testmarvel.EntidadesComicsJson.Price;
import com.example.cristian.testmarvel.EntidadesComicsJson.Series;
import com.example.cristian.testmarvel.EntidadesComicsJson.Stories;
import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by grite on 10/2/2017.
 */

public class Comic extends SugarRecord {
    private long idcomic;
    private String title;
    private String description;
    private long pageCount;
    private String resourceURI;
    private String modified; //Fecha

    private double price;
    private String imageLocalPath = null;
    private String creators;
    private String characters;
    private String series;
    private int favorito = ComicsListAdapter.TAG_NO_FAVORITO;
    public Comic(){

    }

    public long getIdComic() {
        return idcomic;
    }

    public void setIdComic(long idComic) {
        this.idcomic = idComic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImageLocalPath() {
        return imageLocalPath;
    }

    public void setImageLocalPath(String imageLocalPath) {
        this.imageLocalPath = imageLocalPath;
    }

    public String getCreators() {
        return creators;
    }

    public void setCreators(String creators) {
        this.creators = creators;
    }

    public String getCharacters() {
        return characters;
    }

    public void setCharacters(String characters) {
        this.characters = characters;
    }



    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
