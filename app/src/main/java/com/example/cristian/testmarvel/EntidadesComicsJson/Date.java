
package com.example.cristian.testmarvel.EntidadesComicsJson;


import com.orm.SugarRecord;

public class Date {

    private String type;
    private String date;

    public Date(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
