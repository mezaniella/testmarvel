package com.example.cristian.testmarvel.Fragments;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.dinuscxj.progressbar.CircleProgressBar;
import com.example.cristian.testmarvel.Adapters.ComicsFavoritosListAdapters;
import com.example.cristian.testmarvel.Adapters.ComicsListAdapter;
import com.example.cristian.testmarvel.Clases.Utils;
import com.example.cristian.testmarvel.Entidades.Comic;
import com.example.cristian.testmarvel.EntidadesComicsJson.Pricipal;
import com.example.cristian.testmarvel.EntidadesComicsJson.Result;
import com.example.cristian.testmarvel.Interfaz.MarvelApi;
import com.example.cristian.testmarvel.MainActivity;
import com.example.cristian.testmarvel.R;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Cristian on 11 feb 2017.
 */

public class FavoritosFragments extends Fragment {

    ComicsFavoritosListAdapters comicsListAdapter;
    List<Comic> listComics;

    @BindView(R.id.recycler_comics)
    RecyclerView recyclerView;
    @BindView(R.id.btn_refresh) Button refresh;
    @BindView(R.id.progress_list)
    CircleProgressBar mCustomProgressBar1;
    ValueAnimator animator;
    Activity activity;

    int[] offset_array = new int[]{0,30,60,90,120,150,180,210};

    public static FavoritosFragments newInstance(Bundle arguments){
        FavoritosFragments f = new FavoritosFragments();
        if(arguments != null){
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.list_comics_fragments, container, false);

        ButterKnife.bind(this,view);
        setHasOptionsMenu(true);
        refresh.setVisibility(View.GONE);
        ((MainActivity) activity).getToolbar().setTitle("Favorite Comics");
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        obtenerListComics();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here

        inflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);


        //permite modificar el hint que el EditText muestra por defecto
        searchView.setQueryHint("SEARCH COMICS");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if (comicsListAdapter != null)
                    getFilter(newText);
                return true;
            }
        });




    }

    private void getFilter(String newText) {

        comicsListAdapter.getFilter().filter(newText);
    }

    private void obtenerListComics() {
        simulateProgress();

        listComics = Comic.find(Comic.class,null);

        if (listComics != null){
            crearAdapters();

           

        }

        else {
            Toast.makeText(activity, "No favorites", Toast.LENGTH_SHORT).show();
        }
        pararProgresDialog();


    }




    private void crearAdapters() {

        comicsListAdapter = new ComicsFavoritosListAdapters(listComics,getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(comicsListAdapter);

        LinearLayoutManager lManager = new LinearLayoutManager(getActivity());
        lManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(lManager);

        recyclerView.setAdapter(comicsListAdapter);
        comicsListAdapter.notifyDataSetChanged();

        comicsListAdapter.setOnItemClickListener(new ComicsFavoritosListAdapters.OnClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                ((MainActivity)activity).setFragmentComic(MainActivity.COMIC_FRAGMENTS,comicsListAdapter.getItem(position).getIdComic());
            }

        });

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(recyclerView.getWindowToken(), 0);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });

    }

    private void simulateProgress() {
         animator = ValueAnimator.ofInt(0, 100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int progress = (int) animation.getAnimatedValue();

                mCustomProgressBar1.setProgress(progress);

            }
        });
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setDuration(4000);
        animator.start();
    }

    private void pararProgresDialog() {
        animator.cancel();
        mCustomProgressBar1.setVisibility(View.GONE);
    }
}
