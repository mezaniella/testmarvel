
package com.example.cristian.testmarvel.EntidadesComicsJson;

import java.util.List;

public class Characters {

    private long available;
    private String collectionURI;
    private List<Personaje> items = null;
    private long returned;

    public long getAvailable() {
        return available;
    }

    public void setAvailable(long available) {
        this.available = available;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    public List<Personaje> getItems() {
        return items;
    }

    public void setItems(List<Personaje> items) {
        this.items = items;
    }

    public long getReturned() {
        return returned;
    }

    public void setReturned(long returned) {
        this.returned = returned;
    }

}
