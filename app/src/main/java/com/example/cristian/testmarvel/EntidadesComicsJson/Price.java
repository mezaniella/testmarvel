
package com.example.cristian.testmarvel.EntidadesComicsJson;


import com.orm.SugarRecord;

public class Price extends SugarRecord{

    private String type;
    private double price;

    public Price(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
