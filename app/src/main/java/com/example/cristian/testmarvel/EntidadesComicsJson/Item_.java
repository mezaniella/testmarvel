
package com.example.cristian.testmarvel.EntidadesComicsJson;


import com.orm.SugarRecord;

public class Item_ extends SugarRecord{

    private String resourceURI;
    private String name;
    private String type;

    public Item_(){

    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
