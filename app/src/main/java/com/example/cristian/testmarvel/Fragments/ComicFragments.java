package com.example.cristian.testmarvel.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.cristian.testmarvel.Adapters.ComicsListAdapter;
import com.example.cristian.testmarvel.Clases.Utils;
import com.example.cristian.testmarvel.Entidades.Comic;
import com.example.cristian.testmarvel.EntidadesComicsJson.Characters;
import com.example.cristian.testmarvel.EntidadesComicsJson.Creators;
import com.example.cristian.testmarvel.EntidadesComicsJson.Item;
import com.example.cristian.testmarvel.EntidadesComicsJson.Personaje;
import com.example.cristian.testmarvel.EntidadesComicsJson.Pricipal;
import com.example.cristian.testmarvel.EntidadesComicsJson.Result;
import com.example.cristian.testmarvel.EntidadesComicsJson.Series;
import com.example.cristian.testmarvel.Interfaz.MarvelApi;
import com.example.cristian.testmarvel.MainActivity;
import com.example.cristian.testmarvel.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by grite on 10/2/2017.
 */

public class ComicFragments extends Fragment {
    Activity activity;
    List<Result> listComics;
    @BindView(R.id.imagen) public ImageView imagen;
    @BindView(R.id.titulo) public TextView titulo;
    @BindView(R.id.precio) public TextView precio;
    @BindView(R.id.favorito) public ImageView favorito;
    @BindView(R.id.comic_descripcion) public TextView descripcion;
    @BindView(R.id.comic_fecha) public TextView fecha;
    @BindView(R.id.comic_fPAGINAS) public TextView paginas;
    @BindView(R.id.comic_series) public TextView series;
    @BindView(R.id.comic_creadores) public TextView creadores;
    @BindView(R.id.comic_personajes) public TextView personajes;
    MarvelApi marvel;
    Result comicOnLine = null;
    Comic comicBD = null;
    public final static int MY_WRITE_EXTERNAL_STORAGE = 1;

    public static ComicFragments newInstance(Bundle arguments){
        ComicFragments f = new ComicFragments();
        if(arguments != null){
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.comic_fragments, container, false);

        ButterKnife.bind(this,view);
        favorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               verifyPermission();
            }
        });
         marvel = MarvelApi.retrofit.create(MarvelApi.class);
        ((MainActivity) activity).getToolbar().setTitle("Comics");
        obtenerComic();
        return view;
    }

    private void obtenerComic() {
        try {

            Bundle b = getArguments();
            long idComic = (long) b.get("idComic");
            String id = String.valueOf(idComic);
            List<Comic> comic = Comic.find(Comic.class, "idcomic = ?", id);

            if (comic.size() != 0) {
                comicBD = comic.get(0);
                cargarComicBD();
            } else {
                cargarComicInternet(idComic);
            }

        } catch (Exception e){
            e.getMessage();
            Toast.makeText(activity,"Error loading comics",Toast.LENGTH_LONG).show();
        }

    }

    private void cargarComicInternet(long idComic) {

        String hash = Utils.MD5();
        final Call<Pricipal> call = marvel.getComic(String.valueOf(idComic),Utils.ts,Utils.clavePublica,hash);

        call.enqueue(new Callback<Pricipal>() {
            @Override
            public void onResponse(Call<Pricipal> call, Response<Pricipal> response) {

                try {

                    if (response.body() != null){
                        listComics = response.body().getData().getResults();
                        comicOnLine = listComics.get(0);
                        setDatosComicsInternet();

                    }



                } catch (Exception e){
                    e.getMessage();
                }

            }
            @Override
            public void onFailure(Call<Pricipal> call, Throwable t) {
                Toast.makeText(getActivity(), "Error loading comics", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setDatosComicsInternet() {

        if (comicOnLine.getImages().size() >0)
            Glide.with(activity).load(comicOnLine.getImages().get(0).getPath()+"."+comicOnLine.getImages().get(0).getExtension())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.loading)

                    .error(R.drawable.error)
                    .into(imagen);
        else Glide.with(activity).load(comicOnLine.getThumbnail().getPath()+"."+comicOnLine.getThumbnail().getExtension())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.loading)

                .error(R.drawable.error)
                .into( imagen)
               ;

        obtenerSeries(comicOnLine.getId());
        titulo.setText(comicOnLine.getTitle());

        if (comicOnLine.getPrices().get(0).getPrice()!=0)
            precio.setText("$ "+String.valueOf(comicOnLine.getPrices().get(0).getPrice()));
        else precio.setText("EXHAUSTED");

        if (comicOnLine.getFavorito() == ComicsListAdapter.TAG_FAVORITO){
            favorito.setImageResource(android.R.drawable.star_big_on);
        } else   {
            favorito.setImageResource(android.R.drawable.star_big_off);
        }

        descripcion.setText((String)comicOnLine.getDescription());
        fecha.setText(comicOnLine.getModified());
        paginas.setText(String.valueOf(comicOnLine.getPageCount()));
        creadores.setText(obtenerCreadores(comicOnLine.getCreators()));
        personajes.setText(obtenerPersonajes(comicOnLine.getCharacters()));
    }

    private void obtenerSeries(long idComic) {

        String hash = Utils.MD5();
        final Call<Pricipal> call = marvel.getSeries(String.valueOf(idComic),Utils.ts,Utils.clavePublica,hash);

        call.enqueue(new Callback<Pricipal>() {
            @Override
            public void onResponse(Call<Pricipal> call, Response<Pricipal> response) {

                try {
                      if (response.body() != null)  {
                        listComics = response.body().getData().getResults();
                        cargarSeries(listComics);}
                    else {
                          series.setText("Not found");
                      }



                } catch (Exception e){
                    e.getMessage();
                }

            }
            @Override
            public void onFailure(Call<Pricipal> call, Throwable t) {
               series.setText("Not found");

            }
        });


    }

    private void cargarSeries(List<Result> listComics) {
        StringBuilder s = new StringBuilder();
        for (Result r : listComics){
            s.append(r.getTitle());
            s.append("\n");
        }

        series.setText(s.toString());
    }

    private String obtenerPersonajes(Characters characters) {
        StringBuilder s = new StringBuilder();

        for (Personaje i : characters.getItems()){
            s.append(i.name);
            s.append("\n");

        }

        return s.toString();
    }

    private String obtenerCreadores(Creators creators) {

        StringBuilder s = new StringBuilder();

        for (Item i : creators.getItems()){
            s.append(i.getName());

            s.append("\n");
        }

        return s.toString();
    }

    private void cargarComicBD() {



        try {
            Bitmap myBitmap = BitmapFactory.decodeFile(comicBD.getImageLocalPath());

            imagen.setImageBitmap(myBitmap);
        }catch (Exception e){
            e.getMessage();


            imagen.setImageResource(R.drawable.error);
        }


        series.setText(comicBD.getSeries());
        titulo.setText(comicBD.getTitle());

        if (comicBD.getPrice()!=0)
            precio.setText("$ "+String.valueOf(comicBD.getPrice()));
        else precio.setText("EXHAUSTED");


            favorito.setImageResource(android.R.drawable.star_big_on);


        descripcion.setText(comicBD.getDescription());
        fecha.setText(comicBD.getModified());
        paginas.setText(String.valueOf(comicBD.getPageCount()));
        creadores.setText(comicBD.getCreators());
        personajes.setText(comicBD.getCharacters());

    }

    public void tratarFavorito(){
        if (comicOnLine != null)
            if (comicOnLine.getFavorito() == ComicsListAdapter.TAG_FAVORITO){
                quitarComicdeFavorito();
            } else agregarComicFavorito();
        else if (comicBD != null){
                if (comicBD.getFavorito() == ComicsListAdapter.TAG_FAVORITO){
                    comicBD.delete();
                    comicBD.setFavorito(ComicsListAdapter.TAG_NO_FAVORITO);
                    Toast.makeText(activity, "REMOVE FAVORITES", Toast.LENGTH_SHORT).show();
                    favorito.setImageResource(android.R.drawable.star_big_off);
                } else {
                    comicBD.setFavorito(ComicsListAdapter.TAG_FAVORITO);
                    comicBD.save();
                    Toast.makeText(activity, "ADDED FAVORITES", Toast.LENGTH_SHORT).show();
                    favorito.setImageResource(android.R.drawable.star_big_on);
                }
        }

    }

    private void agregarComicFavorito() {

        comicOnLine.setFavorito(ComicsListAdapter.TAG_FAVORITO);
        Comic com = new Comic();
        com.setIdComic(comicOnLine.getId());
        com.setTitle(comicOnLine.getTitle());
        com.setPrice(comicOnLine.getPrices().get(0).getPrice());
        if (comicOnLine.getDescription() != null)
         com.setDescription(comicOnLine.getDescription().toString());
        com.setModified(comicOnLine.getModified());
        com.setPageCount(comicOnLine.getPageCount());
        com.setSeries(series.getText().toString());
        com.setCreators(creadores.getText().toString());
        com.setCharacters(personajes.getText().toString());
        com.setFavorito(ComicsListAdapter.TAG_FAVORITO);
        imagen.buildDrawingCache();
        Bitmap b = Bitmap.createBitmap(imagen.getDrawingCache());

        String pathImagen = guardarImagen(String.valueOf(comicOnLine.getId()),b);
        com.setImageLocalPath(pathImagen);
        com.save();

        Toast.makeText(activity, "ADDED FAVORITES", Toast.LENGTH_SHORT).show();
        favorito.setImageResource(android.R.drawable.star_big_on);
    }

    private void quitarComicdeFavorito() {

        comicOnLine.setFavorito(ComicsListAdapter.TAG_NO_FAVORITO);
        List<Comic> listComic = Comic.find(Comic.class,"idComic = ?",String.valueOf(comicOnLine.getId()));
        if(listComic.size() >0){
            listComic.get(0).delete();
            favorito.setImageResource(android.R.drawable.star_big_off);
            Toast.makeText(activity, "REMOVE FAVORITES", Toast.LENGTH_SHORT).show();
        }
    }

    private String guardarImagen (String nombre, Bitmap imagen){

        String pathbase = Environment.getExternalStorageDirectory().getAbsolutePath()+"/MarvelImage";

        try{
        File dirImages = new File(pathbase);
            if (!dirImages.exists())
                dirImages.mkdirs();

        File myPath = new File(dirImages, nombre + ".jpg" );

        FileOutputStream fos = new FileOutputStream(myPath);

            imagen.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();

            return myPath.getAbsolutePath();
        }catch (FileNotFoundException ex){
            ex.getMessage();
        }catch (IOException ex){
            ex.getMessage();
        }
        return null;
    }

    private void verifyPermission()
    {

        int writePermission = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            writePermission = activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED){
            requestPermission();
        }
        else {
            tratarFavorito();
        }
    }

    private void requestPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showSnackBar();
        }else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    private void showSnackBar() {

        Snackbar.make(activity.findViewById(R.id.coordinator),R.string.permission_write_storage,Snackbar.LENGTH_LONG)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override public void onClick(View view){
                        openSettings();
                    }}).show();
    }

    private void openSettings() {

            if (activity == null) {
                return;
            }
            final Intent i = new Intent();
            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            i.addCategory(Intent.CATEGORY_DEFAULT);
            i.setData(Uri.parse("package:" + activity.getPackageName()));
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
             activity.startActivity(i);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                tratarFavorito();
            }else {
                showSnackBar();
            }
        }
    }




}
