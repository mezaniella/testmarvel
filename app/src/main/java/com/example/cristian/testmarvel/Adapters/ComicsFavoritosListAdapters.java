package com.example.cristian.testmarvel.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.cristian.testmarvel.Entidades.Comic;
import com.example.cristian.testmarvel.EntidadesComicsJson.Result;
import com.example.cristian.testmarvel.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Cristian on 11 feb 2017.
 */

public class ComicsFavoritosListAdapters extends RecyclerView.Adapter<ComicsFavoritosListAdapters.ComicsViewHolder> implements Filterable {
    private List<Comic> items;
    private List<Comic> filterable;
    Context context;
    private static ComicsFavoritosListAdapters.OnClickListener clickListener;

    Filter filter;

    @Override
    public int getItemCount() {
        return filterable.size();
    }

    @Override
    public ComicsFavoritosListAdapters.ComicsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_view_item_comic, viewGroup, false);

        return new ComicsFavoritosListAdapters.ComicsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ComicsFavoritosListAdapters.ComicsViewHolder viewHolder, int i) {

        Comic comic = filterable.get(i);
        try {
            Bitmap myBitmap = BitmapFactory.decodeFile(comic.getImageLocalPath());

            viewHolder.imagen.setImageBitmap(myBitmap);
        }catch (Exception e) {
            e.getMessage();
            viewHolder.imagen.setImageResource(R.drawable.error);

        }



        viewHolder.titulo.setText(comic.getTitle());

        if (comic.getPrice()!=0)
            viewHolder.precio.setText("$ "+String.valueOf(comic.getPrice()));
        else viewHolder.precio.setText("AGOTADO");



        if (comic.getFavorito()==ComicsListAdapter.TAG_FAVORITO)
            viewHolder.favorito.setImageResource(android.R.drawable.star_big_on);
        else   {
            viewHolder.favorito.setVisibility(View.INVISIBLE);
        }


    }

    public void setOnItemClickListener(ComicsFavoritosListAdapters.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public Comic getItem(int position) {
        return this.filterable.get(position);
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new ComicsFavoritosListAdapters.ComicsFilter();
        return filter;
    }

    public interface OnClickListener {
        void onItemClick(int position, View v);


    }
    public void clearItemsAll() {
        if (filterable != null){
            filterable.clear();
            notifyDataSetChanged();
        }
    }



    public ComicsFavoritosListAdapters(List<Comic> items,Context context) {
        this.items = items;
        this.context = context;
        this.filterable = items;
    }


    public static class ComicsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.imagen) public ImageView imagen;
        @BindView(R.id.titulo) public TextView titulo;
        @BindView(R.id.precio) public TextView precio;
        @BindView(R.id.favorito) public ImageView favorito;

        public ComicsViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
            v.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    private class ComicsFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String buscar = ((String) constraint).toUpperCase();
            List<Comic> add = new ArrayList<>();
            FilterResults results1 = new FilterResults();

            if (buscar.length() >0){

                for (Comic r : items){
                    if (r.getTitle().toUpperCase().contains(buscar))
                        add.add(r);
                }
                results1.values = add;
                results1.count = add.size();

            } else {
                results1.values = items;
                results1.count = items.size();
            }

            return results1;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterable = (List<Comic>) results.values;
            notifyDataSetChanged();
        }
    }




}
